var app = angular.module('app', []);

angular.module("app", []).controller("list", ["$scope", "$http",
    function ($scope, $http) {
        // Get data
        $scope.start = function() {
            $http({
                method: 'GET',
                url: 'http://esense2.voicetel.pl:20005/getResults?firstName=voicetel&lastName=test',
            }).then(function(response) {
                $scope.data = response;
                $scope.getList('', '');
            }, function(response) {
                document.getElementById('alert').style.display = block;
            });
        };

        // Display list
        $scope.sortReverse = false;
        $scope.getList = function(sort, filterText) {
            // Sort and filter variables
            $scope.sortType = sort;
            $scope.filterText = filterText;

            // Display data in loop
            $scope.dataList = [];
            for (var i = 0; i <  $scope.data.data.length; i++) {
                $scope.dataList.push({  
                    'firstName': $scope.data.data[i].imie,
                    'lastName': $scope.data.data[i].nazwisko,
                    'forward': $scope.data.data[i].forward,
                    'number': $scope.data.data[i].number,
                    'date': $scope.data.data[i].dodano
                });
            }
        };
    }
])